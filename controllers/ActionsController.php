<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

class ActionsController extends Controller{

  public function actions(){

      return [

          'helloworld'=>[
                'class'=>'app\actions\HelloWorldAction',
          ],

          'clculadora'=>[
                'class'=>'app\actions\CalculadoraAction',
          ],
          ];
  }

}

 ?>

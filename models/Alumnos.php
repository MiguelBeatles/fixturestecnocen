<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Alumnos".
 *
 * @property integer $boleta
 * @property string $nombre
 * @property string $apellidop
 * @property string $apellidom
 * @property string $email
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Alumnos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['boleta', 'nombre', 'apellidop', 'apellidom', 'email'], 'required'],
            [['boleta'], 'integer'],
            [['nombre', 'apellidop', 'apellidom'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'boleta' => 'Boleta',
            'nombre' => 'Nombre',
            'apellidop' => 'Apellidop',
            'apellidom' => 'Apellidom',
            'email' => 'Email',
        ];
    }
}

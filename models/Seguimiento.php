<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seguimiento".
 *
 * @property integer $id
 * @property integer $id_siniestro
 * @property string $comentario
 * @property string $estatus
 * @property string $observacion_estatus
 * @property string $tipo_documento
 * @property integer $id_creador
 * @property string $archivo
 * @property string $path
 * @property string $fecha_cita
 * @property string $fecha_alta
 * @property string $fecha_actualizacion
 *
 * @property Siniestro $idSiniestro
 * @property User $idCreador
 */
class Seguimiento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seguimiento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_siniestro', 'comentario', 'estatus', 'id_creador', 'archivo', 'fecha_alta', 'fecha_actualizacion'], 'required'],
            [['id_siniestro', 'id_creador'], 'integer'],
            [['comentario'], 'string'],
            [['fecha_alta', 'fecha_actualizacion'], 'safe'],
            [['estatus', 'observacion_estatus', 'tipo_documento', 'fecha_cita'], 'string', 'max' => 50],
            [['archivo', 'path'], 'string', 'max' => 255],
            [['id_siniestro'], 'exist', 'skipOnError' => true, 'targetClass' => Siniestro::className(), 'targetAttribute' => ['id_siniestro' => 'id']],
            [['id_creador'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_creador' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_siniestro' => 'Id Siniestro',
            'comentario' => 'Comentario',
            'estatus' => 'Estatus',
            'observacion_estatus' => 'Observacion Estatus',
            'tipo_documento' => 'Tipo Documento',
            'id_creador' => 'Id Creador',
            'archivo' => 'Archivo',
            'path' => 'Path',
            'fecha_cita' => 'Fecha Cita',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSiniestro()
    {
        return $this->hasOne(Siniestro::className(), ['id' => 'id_siniestro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCreador()
    {
        return $this->hasOne(User::className(), ['id' => 'id_creador']);
    }
}

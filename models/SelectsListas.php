<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "selects_listas".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $vista
 * @property integer $localizacion
 * @property integer $egresos
 */
class SelectsListas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'selects_listas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'vista', 'localizacion', 'egresos'], 'required'],
            [['localizacion', 'egresos'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['vista'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'vista' => 'Vista',
            'localizacion' => 'Localizacion',
            'egresos' => 'Egresos',
        ];
    }
}

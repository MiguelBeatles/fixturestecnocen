<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $nombre
 * @property string $telefono
 * @property string $extension
 * @property string $celular
 * @property string $direccion
 * @property string $puesto
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $path_avatar
 * @property string $email
 * @property string $horario_atencion
 * @property integer $status
 * @property string $territorio
 * @property string $estado
 * @property string $sucursal
 * @property string $latitud
 * @property string $longitud
 * @property string $color
 * @property string $tipo_atencion
 * @property string $created_at
 * @property integer $updated_at
 *
 * @property Agenda[] $agendas
 * @property Agenda[] $agendas0
 * @property Agenda[] $agendas1
 * @property AgendaFecha[] $agendaFechas
 * @property Seguimiento[] $seguimientos
 * @property Tramite[] $tramites
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'nombre', 'telefono', 'celular', 'direccion', 'puesto', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['status', 'updated_at'], 'integer'],
            [['created_at'], 'safe'],
            [['username', 'nombre', 'password_hash', 'password_reset_token', 'path_avatar', 'email'], 'string', 'max' => 255],
            [['telefono', 'celular'], 'string', 'max' => 55],
            [['extension'], 'string', 'max' => 10],
            [['direccion', 'horario_atencion'], 'string', 'max' => 500],
            [['puesto'], 'string', 'max' => 100],
            [['auth_key'], 'string', 'max' => 32],
            [['territorio', 'estado', 'sucursal', 'latitud', 'longitud', 'color', 'tipo_atencion'], 'string', 'max' => 45],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'nombre' => 'Nombre',
            'telefono' => 'Telefono',
            'extension' => 'Extension',
            'celular' => 'Celular',
            'direccion' => 'Direccion',
            'puesto' => 'Puesto',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'path_avatar' => 'Path Avatar',
            'email' => 'Email',
            'horario_atencion' => 'Horario Atencion',
            'status' => 'Status',
            'territorio' => 'Territorio',
            'estado' => 'Estado',
            'sucursal' => 'Sucursal',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
            'color' => 'Color',
            'tipo_atencion' => 'Tipo Atencion',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendas()
    {
        return $this->hasMany(Agenda::className(), ['id_actualizador' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendas0()
    {
        return $this->hasMany(Agenda::className(), ['id_analista' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendas1()
    {
        return $this->hasMany(Agenda::className(), ['id_creador' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendaFechas()
    {
        return $this->hasMany(AgendaFecha::className(), ['id_creador' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeguimientos()
    {
        return $this->hasMany(Seguimiento::className(), ['id_creador' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTramites()
    {
        return $this->hasMany(Tramite::className(), ['id_creador' => 'id']);
    }
}

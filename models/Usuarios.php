<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Usuarios".
 *
 * @property string $nombre
 * @property string $apellido
 * @property string $email
 * @property integer $edad
 * @property integer $boleta
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Usuarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido', 'email', 'edad', 'boleta'], 'required'],
            [['edad', 'boleta'], 'integer'],
            [['nombre', 'apellido', 'email'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'email' => 'Email',
            'edad' => 'Edad',
            'boleta' => 'Boleta',
        ];
    }
}

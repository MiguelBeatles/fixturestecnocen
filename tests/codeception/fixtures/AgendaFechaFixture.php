<?php
namespace  tests\codeception\fixtures;

use yii\test\ActiveFixture;

class AgendaFechaFixture extends ActiveFixture
{
    public $modelClass = 'app\models\AgendaFecha';
     public $depends = [
    	'tests\codeception\fixtures\AgendaFixture',
    	'tests\codeception\fixtures\UserFixture'
    ];


}

<?php
namespace  tests\codeception\fixtures;

use yii\test\ActiveFixture;

class AgendaFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Agenda';
    public $depends = [
    	'tests\codeception\fixtures\SiniestroFixture',
    	'tests\codeception\fixtures\UserFixture'
    ];

}

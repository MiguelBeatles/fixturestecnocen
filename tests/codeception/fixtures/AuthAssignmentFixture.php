<?php
namespace  tests\codeception\fixtures;

use yii\test\ActiveFixture;

class AuthAssignmentFixture extends ActiveFixture
{
    public $modelClass = 'app\models\AuthAssignment';
     public $depends = [
    	'tests\codeception\fixtures\AuthItemFixture'
    ];

}
<?php
namespace  tests\codeception\fixtures;

use yii\test\ActiveFixture;

class AuthItemChildFixture extends ActiveFixture
{
    public $modelClass = 'app\models\AuthItemChild';
    public $depends = [
    	'tests\codeception\fixtures\AuthItemFixture'
    ];


}

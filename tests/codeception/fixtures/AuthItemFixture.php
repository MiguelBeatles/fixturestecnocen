<?php
namespace  tests\codeception\fixtures;

use yii\test\ActiveFixture;

class AuthItemFixture extends ActiveFixture
{
    public $modelClass = 'app\models\AuthItem';

     public $depends = [
    	'tests\codeception\fixtures\AuthRuleFixture'
    ];


}
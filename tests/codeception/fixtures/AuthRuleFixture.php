<?php
namespace  tests\codeception\fixtures;

use yii\test\ActiveFixture;

class AuthRuleFixture extends ActiveFixture
{
    public $modelClass = 'app\models\AuthRule';

}
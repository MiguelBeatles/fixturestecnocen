<?php
//namespace app\tests\codeception\unit\fixtures;
namespace  tests\codeception\fixtures;

use yii\test\ActiveFixture;

class CountryFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Country';

}

<?php
namespace  tests\codeception\fixtures;

use yii\test\ActiveFixture;

class SeguimientoFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Seguimiento';
     public $depends = [
    	'tests\codeception\fixtures\SiniestroFixture',
    	'tests\codeception\fixtures\UserFixture'
    ];


}
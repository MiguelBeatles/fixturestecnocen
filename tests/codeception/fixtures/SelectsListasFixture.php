<?php
namespace  tests\codeception\fixtures;

use yii\test\ActiveFixture;

class SelectsListasFixture extends ActiveFixture
{
    public $modelClass = 'app\models\SelectsListas';

}
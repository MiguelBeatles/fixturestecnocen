<?php
namespace  tests\codeception\fixtures;

use yii\test\ActiveFixture;

class TramiteFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Tramite';
    public $depends = [
    	'tests\codeception\fixtures\SiniestroFixture',
    	'tests\codeception\fixtures\UserFixture'
    ];

}
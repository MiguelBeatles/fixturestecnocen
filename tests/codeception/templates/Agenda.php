<?php
/**
 * @var \Faker\Faker $faker
 */

    return [ 

    'id_siniestro' => $faker->randomNumber($nbDigits = NULL),
      'id_creador' => $faker->randomNumber($nbDigits = NULL),
      'id_actualizador' => $faker->randomNumber($nbDigits = NULL),
      'id_analista' => $faker->randomNumber($nbDigits = NULL),
      'titulo' => $faker->sentence($nbWords = 3, $variableNbWords =NULL) ,
      'fecha_inicio_cita'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s') ,
      'fecha_fin_cita'=>$faker->dateTime()->format('Y-m-d H:i:s'),
      'eliminado'=>$faker->randomDigit,
      'fecha_borrado'=>$faker->dateTime()->format('Y-m-d H:i:s'),
      'fecha_alta'=>$faker->dateTime()->format('Y-m-d H:i:s'),
      'fecha_actualizacion'=>$faker->dateTime()->format('Y-m-d H:i:s')
  
];


/*$connection = new yii\db\Connection([
    'dsn' => 'mysql:host=localhost;dbname=sidol',
    'username' => 'root',
    'password' => '4612029',
    'charset' => 'utf8',
]);


//$connection->createCommand()->truncateTable('Alumnos')->execute();

$connection->createCommand()->batchInsert('agenda', ['id_siniestro', 
	'id_creador','id_actualizador','id_analista','titulo','fecha_inicio_cita',
	'fecha_fin_cita','eliminado','fecha_borrado','fecha_alta',
	'fecha_actualizacion'], [
    ['id_siniestro' => $faker->randomNumber($nbDigits = NULL),
      'id_creador' => $faker->randomNumber($nbDigits = NULL),
      'id_actualizador' => $faker->randomNumber($nbDigits = NULL),
      'id_analista' => $faker->randomNumber($nbDigits = NULL),
      'titulo' => $faker->sentence($nbWords = 3, $variableNbWords =NULL) ,
      'fecha_inicio_cita'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s') ,
      'fecha_fin_cita'=>$faker->dateTime()->format('Y-m-d H:i:s'),
      'eliminado'=>$faker->randomDigit,
      'fecha_borrado'=>$faker->dateTime()->format('Y-m-d H:i:s'),
      'fecha_alta'=>$faker->dateTime()->format('Y-m-d H:i:s'),
      'fecha_actualizacion'=>$faker->dateTime()->format('Y-m-d H:i:s')]
])->execute();*/


<?php
/**
 * @var \Faker\Faker $faker
 */

 return [ 

    'id_agenda'  => $faker->randomNumber($nbDigits = NULL),
      'id_creador' => $faker->randomNumber($nbDigits = NULL),
      'motivo' => $faker->sentence($nbWords = 3, $variableNbWords =NULL),
      'inicio' =>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s') ,
      'fin' =>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s') ,
      'eliminado'=>$faker->randomDigit,
      'fecha_borrado'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s') ,
      'fecha_alta'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s') ,
      'fecha_actualizacion'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s') ,
  
];


/*$connection = new yii\db\Connection([
    'dsn' => 'mysql:host=localhost;dbname=sidol',
    'username' => 'root',
    'password' => '4612029',
    'charset' => 'utf8',
]);


//$connection->createCommand()->truncateTable('Alumnos')->execute();

$connection->createCommand()->batchInsert('agenda_fecha', ['id_agenda', 'id_creador','motivo','inicio','fin','eliminado','fecha_borrado','fecha_alta','fecha_actualizacion'], [
    ['id_agenda'  => $faker->randomNumber($nbDigits = NULL),
      'id_creador' => $faker->randomNumber($nbDigits = NULL),
      'motivo' => $faker->sentence($nbWords = 3, $variableNbWords =NULL),
      'inicio' =>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s') ,
      'fin' =>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s') ,
      'eliminado'=>$faker->randomDigit,
      'fecha_borrado'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s') ,
      'fecha_alta'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s') ,
      'fecha_actualizacion'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s') ]
])->execute();*/

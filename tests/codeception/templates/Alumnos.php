<?php
/**
 * @var \Faker\Faker $faker
 */


$connection = new yii\db\Connection([
    'dsn' => 'mysql:host=localhost;dbname=alumnos',
    'username' => 'root',
    'password' => '4612029',
    'charset' => 'utf8',
]);


//$connection->createCommand()->truncateTable('Alumnos')->execute();

$connection->createCommand()->batchInsert('Alumnos', ['boleta', 'nombre',
	'apellidop','apellidom','email'], [
    ['boleta' => $faker->unique()->randomNumber($nbDigits = 9),
      'nombre' => $faker->firstName($gender = null|'male'|'female'),
      'apellidop' => $faker->lastName,
      'apellidom' => $faker->lastName,
      'email' => $faker->email]
])->execute();



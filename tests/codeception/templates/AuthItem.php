<?php
/**
 * @var \Faker\Faker $faker
 */

return [

      'name' => $faker->unique()->sentence($nbWords = 3, $variableNbWords = true) ,
      'type' => $faker->randomDigit,
      'description'=> $faker->sentence($nbWords = 3, $variableNbWords = true) ,
      'rule_name'=> $faker->word,
      'data'=> $faker->sentence($nbWords = 3, $variableNbWords = true) ,
      'created_at'=> $faker->randomNumber($nbDigits = NULL),
      'updated_at'=> $faker->randomNumber($nbDigits = NULL)
];

/*
$connection = new yii\db\Connection([
    'dsn' => 'mysql:host=localhost;dbname=sidol',
    'username' => 'root',
    'password' => '4612029',
    'charset' => 'utf8',
]);


//$connection->createCommand()->truncateTable('Alumnos')->execute();

$connection->createCommand()->batchInsert('auth_item', ['name', 'type',
	'description','rule_name','data','created_at','updated_at'], [
    [ 'name' => $faker->text,
      'type' => $faker->randomDigit,
      'description'=> $faker->text,
      'rule_name'=> $faker->text,
      'data'=> $faker->text,
      'created_at'=> $faker->randomNumber($nbDigits = NULL),
      'updated_at'=> $faker->randomNumber($nbDigits = NULL)]
])->execute();
*/
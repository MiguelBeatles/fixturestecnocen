<?php
/**
 * @var \Faker\Faker $faker
 */

return [

	'name' => $faker->unique()->sentence($nbWords = 3, $variableNbWords = true) ,
      'data' => $faker->sentence($nbWords = 3, $variableNbWords = true) ,
      'created_at'=> $faker->randomNumber($nbDigits = NULL),
      'updated_at'=> $faker->randomNumber($nbDigits = NULL)

];



/*
$connection = new yii\db\Connection([
    'dsn' => 'mysql:host=localhost;dbname=sidol',
    'username' => 'root',
    'password' => '4612029',
    'charset' => 'utf8',
]);


//$connection->createCommand()->truncateTable('Alumnos')->execute();

$connection->createCommand()->batchInsert('auth_rule', 
  ['name', 'data','created_at','updated_at'], [
    [ 'name' => $faker->unique()->word,
      'data' => $faker->text,
      'created_at'=> $faker->randomNumber($nbDigits = NULL),
      'updated_at'=> $faker->randomNumber($nbDigits = NULL)]
])->execute();
*/
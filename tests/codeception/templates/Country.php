<?php
/**
 * @var \Faker\Faker $faker
 */

return [
      'code' => $faker->unique()->stateAbbr,
      'name' => $faker->randomLetter,
      'population' => $faker->randomNumber()
];

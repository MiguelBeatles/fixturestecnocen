<?php
// load fixtures
$this->tester->haveFixtures([
    'user' => [
        'class' => UserFixture::className(),
        // fixture data located in tests/_data/user.php
        'dataFile' => codecept_data_dir().'User.php'
    ],

    'sinestro' [
    	'class'=>SiniestroFixture::clasName(),
    	'dataFilea'=> codecept_data_dir().'Siniestro.php'
    ],

    'agenda' [
    	'class'=>AgendaFixture::clasName(),
    	'dataFilea'=> codecept_data_dir().'Agenda.php'
    ],

    'agendafecha' [
    	'class'=>AgendaFechaFixture::clasName(),
    	'dataFilea'=> codecept_data_dir().'AgendaFecha.php'
    ],

    'seguimiento' [
    	'class'=>SeguimientoFixture::clasName(),
    	'dataFilea'=> codecept_data_dir().'Seguimiento.php'
    ],

    'tramite' [
    	'class'=>TramiteFixture::clasName(),
    	'dataFilea'=> codecept_data_dir().'Tramite.php'
    ],

    'selectslistas' [
    	'class'=>SelectsListasFixture::clasName(),
    	'dataFilea'=> codecept_data_dir().'SelectsListas.php'
    ],

    'AuthRule' [
    	'class'=>AuthRuleFixture::clasName(),
    	'dataFilea'=> codecept_data_dir().'AuthRule.php'
    ],

    'AuthItem' [
    	'class'=>AuthItemFixture::clasName(),
    	'dataFilea'=> codecept_data_dir().'AuthItem.php'
    ],

    'AuthItemChild' [
    	'class'=>AuthItemChildFixture::clasName(),
    	'dataFilea'=> codecept_data_dir().'AuthItemChild.php'
    ],

    'AuthAssignment' [
    	'class'=>AuthAssignmentFixture::clasName(),
    	'dataFilea'=> codecept_data_dir().'AuthAssignment.php'
    ]
]);
<?php
/**
 * @var \Faker\Faker $faker
 */

return [
  'id_siniestro'=>$faker->randomNumber($nbDigits = NULL),
     'comentario'=>$faker->text,
     'estatus'=>$faker->sentence($nbWords = 2, $variableNbWords = true),
     'observacion_estatus'=>$faker->text,
     'tipo_documento'=>$faker->text,
     'id_creador'=>$faker->randomNumber($nbDigits = NULL),
     'archivo'=>$faker->text,
     'path'=>$faker->text,
     'fecha_cita'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s'),
     'fecha_alta'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s'),
     'fecha_actualizacion'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s')
];

/*
$connection = new yii\db\Connection([
    'dsn' => 'mysql:host=localhost;dbname=sidol',
    'username' => 'root',
    'password' => '4612029',
    'charset' => 'utf8',
]);


//$connection->createCommand()->truncateTable('Alumnos')->execute();

$connection->createCommand()->batchInsert('seguimiento', 
  ['id_siniestro','comentario','estatus','observacion_estatus',
  'tipo_documento','id_creador','archivo','path','fecha_cita',
  'fecha_alta','fecha_actualizacion'], [
    ['id_siniestro'=>$faker->randomNumber($nbDigits = NULL),
     'comentario'=>$faker->text,
     'estatus'=>$faker->sentence($nbWords = 2, $variableNbWords = true),
     'observacion_estatus'=>$faker->text,
  	 'tipo_documento'=>$faker->text,
  	 'id_creador'=>$faker->randomNumber($nbDigits = NULL),
  	 'archivo'=>$faker->text,
  	 'path'=>$faker->text,
  	 'fecha_cita'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s'),
  	 'fecha_alta'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s'),
  	 'fecha_actualizacion'=>$faker->dateTimeThisYear($max = 'now')->format('Y-m-d H:i:s')]
])->execute();
*/
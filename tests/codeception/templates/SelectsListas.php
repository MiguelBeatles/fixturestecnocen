<?php
/**
 * @var \Faker\Faker $faker
 */

return [
	'nombre' => $faker->word,
      'vista' => $faker->word,
      'localizacion'=> $faker->numberBetween($min = 0, $max = 1), // 8567,
      'egresos'=> $faker->numberBetween($min = 0, $max = 1)
];

/*$connection = new yii\db\Connection([
    'dsn' => 'mysql:host=localhost;dbname=sidol',
    'username' => 'root',
    'password' => '4612029',
    'charset' => 'utf8',
]);


//$connection->createCommand()->truncateTable('Alumnos')->execute();

$connection->createCommand()->batchInsert('selects_listas', 
  ['nombre', 'vista','localizacion','egresos'], [
    [ 'nombre' => $faker->word,
      'vista' => $faker->word,
      'localizacion'=> $faker->numberBetween($min = 0, $max = 1), // 8567,
      'egresos'=> $faker->numberBetween($min = 0, $max = 1) ]
])->execute();
*/
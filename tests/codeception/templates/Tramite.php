<?php
/**
 * @var \Faker\Faker $faker
 */


return[

  'id_siniestro' => $faker->randomNumber($nbDigits = NULL),
      'id_creador' => $faker->randomNumber($nbDigits = NULL),
      'concepto'=> $faker->text,
      'monto'=> $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = NULL),
      'fecha_alta'=>$faker->dateTimeThisDecade($max = 'now')->format('Y-m-d H:i:s'),
      'fecha_actualizacion'=>$faker->dateTimeThisDecade($max = 'now')->format('Y-m-d H:i:s')
];

/*
$connection = new yii\db\Connection([
    'dsn' => 'mysql:host=localhost;dbname=sidol',
    'username' => 'root',
    'password' => '4612029',
    'charset' => 'utf8',
]);


//$connection->createCommand()->truncateTable('Alumnos')->execute();

$connection->createCommand()->batchInsert('tramite', 
  ['id_siniestro', 'id_creador','concepto','monto','fecha_alta',
  'fecha_actualizacion'], [
    [ 'id_siniestro' => $faker->randomNumber($nbDigits = NULL),
      'id_creador' => $faker->randomNumber($nbDigits = NULL),
      'concepto'=> $faker->text,
      'monto'=> $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = NULL),
      'fecha_alta'=>$faker->dateTimeThisDecade($max = 'now')->format('Y-m-d H:i:s'),
      'fecha_actualizacion'=>$faker->dateTimeThisDecade($max = 'now')->format('Y-m-d H:i:s')]
])->execute();
*/
<?php
/**
 * @var \Faker\Faker $faker
 */

return [

    'username'=>$faker->unique()->userName,
     'nombre'=>$faker->unique()->name($gender = null|'male'|'female') ,
     'telefono'=>$faker->phoneNumber,
     'extension'=>$faker->randomNumber($nbDigits = 4),
     'celular'=>$faker->randomNumber($nbDigits = NULL), // 79907610,
     'direccion'=>$faker->address,
     'puesto'=>$faker->randomElement($array = array ('Dir Desarrollo de Software','N/A','Localizacion y Contacto',
     'analista de localizacion','Analista de Localizacion y Contacto','LYC','Analista de siniestros',
     'Coordinacion Localizacion y contacto','Localizador','Supervisor','Prueba','Analista')),
     'auth_key'=>Yii::$app->security->generateRandomString(),
     'password_hash'=>Yii::$app->getSecurity()->generatePasswordHash($faker->password),
     'password_reset_token'=>$faker->unique()->randomNumber($nbDigits = NULL),
     'path_avatar'=>$faker->word,
     'email'=>$faker->unique()->email,
     'horario_atencion'=>$faker->time($format = 'H:i:s', $max = 'now'),
     'status'=>$faker->numberBetween($min = 0, $max = 10),
     'territorio'=>$faker->cityPrefix ,
     'estado'=>$faker->state,
     'sucursal'=>$faker->company,
     'latitud'=>$faker->latitude($min = -90, $max = 90),
     'longitud'=>$faker->longitude($min = -180, $max = 180) ,
     'color'=>$faker->hexcolor,
     'tipo_atencion'=>$faker->text,
     'created_at'=>$faker->dateTimeThisDecade($max = 'now')->format('Y-m-d H:i:s'),
     'updated_at'=>$faker->dateTimeThisDecade($max = 'now')->format('Y-m-d H:i:s')
];


/*$connection = new yii\db\Connection([
    'dsn' => 'mysql:host=localhost;dbname=sidol',
    'username' => 'root',
    'password' => '4612029',
    'charset' => 'utf8',
]);


//$connection->createCommand()->truncateTable('user')->execute();

$connection->createCommand()->batchInsert('user', 
  ['username', 'nombre','telefono','extension','celular','direccion','puesto',
  'auth_key','password_hash','password_reset_token','path_avatar','email',
  'horario_atencion','status','territorio','estado','sucursal','latitud',
  'longitud','color','tipo_atencion','created_at','updated_at'], [
    ['username'=>$faker->unique()->userName,
     'nombre'=>$faker->unique()->email,
     'telefono'=>$faker->phoneNumber,
     'extension'=>$faker->randomNumber($nbDigits = 4),
     'celular'=>$faker->randomNumber($nbDigits = NULL), // 79907610,
     'direccion'=>$faker->address,
     'puesto'=>$faker->jobTitle,
     'auth_key'=>$faker->uuid,
     'password_hash'=>$faker->password,
     'password_reset_token'=>$faker->unique()->randomNumber($nbDigits = NULL),
     'path_avatar'=>$faker->word,
     'email'=>$faker->email,
     'horario_atencion'=>$faker->time($format = 'H:i:s', $max = 'now'),
     'status'=>$faker->numberBetween($min = 0, $max = 10),
     'territorio'=>$faker->cityPrefix ,
     'estado'=>$faker->state,
     'sucursal'=>$faker->company,
     'latitud'=>$faker->latitude($min = -90, $max = 90),
     'longitud'=>$faker->longitude($min = -180, $max = 180) ,
     'color'=>$faker->hexcolor,
     'tipo_atencion'=>$faker->text,
     'created_at'=>$faker->dateTimeThisDecade($max = 'now')->format('Y-m-d H:i:s'),
     'updated_at'=>$faker->dateTimeThisDecade($max = 'now')->format('Y-m-d H:i:s')]
])->execute();

*/
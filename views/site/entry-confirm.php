<?php
use yii\helpers\Html;
use yii\swiftmailer\Mailer;
 ?>

 <p>Ingresa los siguientes datos:</p>

   <ul>
<li><label>Nombre</label>:<?= Html::encode($model->name)?></li>
<li><label>Correo</label>:<?= Html::encode($model->email)?></li>
<li><label>Titulo</label>:<?= Html::encode($model->title)?></li>
<li><label>Mensaje</label>:<?= Html::encode($model->mensaje)?></li>
   </ul>

   <?php
   Yii::$app->mailer->compose()
       ->setFrom('mmejia@tecnocen.com')
       ->setTo('mmejia@tecnocen.com')
       ->setSubject('Message subject')
       ->setTextBody('Plain text content')
       ->setHtmlBody('<b>HTML content</b>')
       ->send();


       ?>

<?php
use yii\helpers\Html;
 ?>

 <p>Ingresa los siguientes datos:</p>

   <ul>
<li><label>Nombre</label>:<?= Html::encode($model->name)?></li>
<li><label>Correo</label>:<?= Html::encode($model->email)?></li>
<li><label>Titulo</label>:<?= Html::encode($model->title)?></li>
<li><label>Contenido</label>:<?= Html::encode($model->mensaje)?></li>
   </ul>
